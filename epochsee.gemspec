# frozen_string_literal: true

require_relative "lib/epochsee/version"

Gem::Specification.new do |spec|
  spec.name = "epochsee"
  spec.version = Epochsee::VERSION
  spec.authors = ["Kjetil Brevik"]
  spec.email = ["kbrevik@gmail.com"]

  spec.summary = "Converts Unix timestamps to date"
  spec.description = "Utility to convert unix timestamp to ISO-8601 format"
  spec.homepage = "https://gitlab.com/kbrevik/epochsee"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
