# frozen_string_literal: true

require_relative "epochsee/version"
require_relative "epochsee/options"
require_relative "epochsee/timestamp"

module Epochsee
  class Error < StandardError; end

  def self.run
    options = Epochsee::Options.run
    epoch = options.timestamp.to_i

    if epoch < 0
      puts "Only positive integers are supported at this time"
      exit
    end

    timestamp = Epochsee::Timestamp.new(epoch)
    puts "#{timestamp}"
  end

  def self.leap_year?(year)
    if (year % 100 == 0) && (year % 400 != 0)
      return false
    end
    if (year % 4 == 0) || (year % 400 == 0)
      return true
    end
    false
  end


end
