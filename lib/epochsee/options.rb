require 'optparse'

module Epochsee
  class Options
    BANNER = 'Usage epochsee [options]'.freeze

    attr_accessor :timestamp

    def initialize
      @timestamp = ''
    end

    def self.run
      options = Options.new
      OptionParser.new do |parser|
        options.define_options(parser)
        parser.parse!
      end
      return options
    end

    def define_options(parser)
      parser.banner = BANNER
      parse_timestamp(parser)
      version(parser)
      help(parser)
    end

    def parse_timestamp(parser)
      parser.on("-t=integer", "--timestamp=integer", "Unix timestamp") do |value|
        @timestamp = value
      end
    end

    def version(parser)
      parser.on("-v", "--version", "Print version") do
        puts Epochsee::VERSION
        exit
      end
    end

    def help(parser)
      parser.on("-h", "--help", "Prints this help") do
        puts parser
        exit
      end
    end
  end
end

