module Epochsee
  class Timestamp

    BASE_YEAR = 1970
    BASE_MONTH = 1
    BASE_DAY = 1
    MONTHS_IN_NORMAL_YEAR = [31,28,31,30,31,30,31,31,30,31,30,31]
    MONTHS_IN_LEAP_YEAR = [31,29,31,30,31,30,31,31,30,31,30,31]

    SECS_PER_DAY = 86400
    SECS_PER_YEAR = 365 * SECS_PER_DAY

    attr_reader :timestamp, :year, :month, :day

    def initialize(timestamp)
      @timestamp = timestamp.to_i
      set_date
    end

    def to_s
      "#{year}-#{month_str}-#{day_str}"
    end

    private

    def month_str
      month.to_s.rjust(2, "0")
    end

    def day_str
      day.to_s.rjust(2, "0")
    end

    def set_date
      @year, remainder = get_year
      @month, remainder = get_month(remainder)
      @day = BASE_DAY + (remainder / SECS_PER_DAY)
    end


    def get_month(seconds_in_year)
      month = BASE_MONTH
      progress = 0
      remainder = 0

      while progress < seconds_in_year
        secs = seconds_in_month(month)
        if (progress + secs) > seconds_in_year
          remainder = seconds_in_year - progress
          break
        else
          progress = progress + secs
          month = month + 1
        end
      end
      return month, remainder
    end

    def seconds_in_month(month)
      if Epochsee.leap_year?(year)
        number_of_days = MONTHS_IN_LEAP_YEAR[month-1]
      else
        number_of_days = MONTHS_IN_NORMAL_YEAR[month-1]
      end
      number_of_days * SECS_PER_DAY
    end

    def seconds_in_year(year)
      if Epochsee.leap_year?(year)
        SECS_PER_YEAR + SECS_PER_DAY
      else
        SECS_PER_YEAR
      end
    end

    def get_year
      year = BASE_YEAR
      progress = 0
      remainder = 0

      while progress < timestamp
        secs = seconds_in_year(year)

        if (progress + secs) > timestamp
          remainder = timestamp - progress
          break
        else
          progress = progress + secs
          year = year + 1
        end
      end
      return year, remainder
    end
  end
end
