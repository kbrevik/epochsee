# frozen_string_literal: true

require "test_helper"

class EpochseeTest < Minitest::Test

  def test_that_it_has_a_version_number
    refute_nil ::Epochsee::VERSION
  end

  def test_it_should_detect_leap_year
    leap_years = [1600, 2000, 2004, 2020, 2024]
    normal_years = [1700, 1900, 1974, 1977]

    leap_years.each do |year|
      assert Epochsee.leap_year?(year), "#{year} should be a leap year"
    end

    normal_years.each do |year|
      refute Epochsee.leap_year?(year), "#{year} should not be a leap year"
    end
  end
end
