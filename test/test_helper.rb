# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "epochsee"
require "date"
require "debug"

require "minitest/autorun"

