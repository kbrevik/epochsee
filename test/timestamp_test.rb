# frozen_string_literal: true

require "test_helper"

class TimestampTest < Minitest::Test

  def test_it_prints_star_wars_release_date
    epoch = Time.utc(1977, 12, 26).to_i
    expected = "1977-12-26"
    date = Epochsee::Timestamp.new(epoch)

    assert_equal expected, date.to_s, "Date is not correct"
  end

  def test_every_day_in_a_normal_year
    dates = Date.civil(1977,1,1)..Date.civil(1977,12,31)

    dates.each do |date|
      epoch = Time.utc(date.year, date.month, date.day).to_i
      timestamp = Epochsee::Timestamp.new(epoch)

      assert_equal "#{date}", "#{timestamp}"
    end
  end

  def test_every_day_in_a_leap_year
    dates = Date.civil(1976,1,1)..Date.civil(1976,12,31)

    dates.each do |date|
      epoch = Time.utc(date.year, date.month, date.day).to_i
      timestamp = Epochsee::Timestamp.new(epoch)

      assert_equal "#{date}", "#{timestamp}"
    end
  end

  def test_every_month_in_a_normal_year
    dates = Date.civil(1977,1,1)..Date.civil(1977,12,31)

    dates.each do |date|
      epoch = Time.utc(date.year, date.month, date.day).to_i
      timestamp = Epochsee::Timestamp.new(epoch)
      assert_equal date.month, timestamp.month
    end
  end

  def test_it_prints_many_years
    (1970..5000).each do |year|
      epoch = Time.utc(year,1,1).to_i
      timestamp = Epochsee::Timestamp.new(epoch)
      assert_equal year, timestamp.year, "#{time} did not print the correct year"
    end
  end

end
